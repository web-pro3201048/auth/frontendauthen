import { useLoadingStore } from './loading'
import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import typeService from '@/services/type'
import type { Role } from '@/types/Role'
import type { Type } from '@/types/Type'


export const userTypeStore = defineStore('type', () => {
  const loadingStore = useLoadingStore()
  const types = ref<Type[]>([])
  const initialtype: Type = {
    name: ''
  }
  const editedtype = ref<Type>(JSON.parse(JSON.stringify(initialtype)))

  async function getType(id: number) {
    loadingStore.doLoad()
    const res = await typeService.getType(id)
    editedtype.value = res.data
    loadingStore.finish()
  }
  async function getTypes() {
    try{
      loadingStore.doLoad()
      const res = await typeService.getTypes()
      types.value = res.data
      loadingStore.finish()
    }catch(e){
      loadingStore.finish()
    }
    
    
  }
  async function saveType() {
    loadingStore.doLoad()
    const type = editedtype.value
    if (!type.id) {
      // Add new
      console.log('Post ' + JSON.stringify(type))
      const res = await typeService.addType(type)
    } else {
      // Update
      console.log('Patch ' + JSON.stringify(type))
      const res = await typeService.updateType(type)
    }

    await getTypes()
    loadingStore.finish()
  }
  async function deleteType() {
    loadingStore.doLoad()
    const type = editedtype.value
    const res = await typeService.delType(type)

    await getTypes()
    loadingStore.finish()
  }

  function clearForm() {
    editedtype.value = JSON.parse(JSON.stringify(initialtype))
  }
  return { types, getTypes, saveType, deleteType, editedtype, getType, clearForm }
})
